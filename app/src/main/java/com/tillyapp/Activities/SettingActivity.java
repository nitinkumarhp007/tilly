package com.tillyapp.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.tillyapp.R;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SettingActivity extends AppCompatActivity {
    SettingActivity context;
    @BindView(R.id.back)
    ImageButton back;
    @BindView(R.id.change_password)
    Button changePassword;
    @BindView(R.id.terms_condition)
    Button termsCondition;
    @BindView(R.id.about_us)
    Button aboutUs;
    @BindView(R.id.logout)
    Button logout;
    @BindView(R.id.my_locations)
    Button myLocations;

    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        context = SettingActivity.this;
        savePref = new SavePref(context);
    }

    @OnClick({R.id.back, R.id.my_locations, R.id.change_password, R.id.terms_condition, R.id.about_us, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.my_locations:
                startActivity(new Intent(this, MyLocationActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password:
                startActivity(new Intent(this, ChangePasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.terms_condition:
                Intent intent1 = new Intent(context, TermConditionActivity.class);
                intent1.putExtra("type", "about");
                startActivity(intent1);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.about_us:
                Intent intent = new Intent(context, PrivacyActivity.class);
                intent.putExtra("type", "term");
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.logout:
                LogoutAlert();
                break;
        }
    }

    private void LogoutAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        savePref.clearPreferences();
                        util.showToast(context, "User Logout Successfully!");
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        util.showToast(context, "User Logout Successfully!");
                        //  LOGOUT_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void LOGOUT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context,
                AllTillyAPIs.LOGOUT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.clearPreferences();
                            util.showToast(context, "User Logout Successfully!");
                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            util.showToast(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
