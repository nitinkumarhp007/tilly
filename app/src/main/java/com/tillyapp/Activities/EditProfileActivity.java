package com.tillyapp.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tillyapp.R;
import com.tillyapp.Util.ConnectivityReceiver;
import com.tillyapp.Util.Parameters;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.back)
    ImageButton back;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.change_profile_pic)
    Button changeProfilePic;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.save)
    Button save;
    @BindView(R.id.user_name)
    EditText userName;

    private int MEDIA_TYPE_GALLERY = 1;
    private int MEDIA_TYPE_CAPTURE = 2;
    private String selectedimage = "";
    Uri fileUri;

    EditProfileActivity context;
    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        context = EditProfileActivity.this;
        savePref = new SavePref(context);

        setdata();
    }

    private void setdata() {
        name.setText(savePref.getName());
        phone.setText(savePref.getPhone());
        email.setText(savePref.getEmail());
        userName.setText(savePref.getUserName());
        if (!savePref.getImage().isEmpty())
            Glide.with(context).load(savePref.getImage()).into(image);
        else
            Glide.with(context).load(R.drawable.user).into(image);
    }


    @OnClick({R.id.back, R.id.image, R.id.save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.image:
                options();
                break;
            case R.id.save:
                if (ConnectivityReceiver.isConnected()) {
                    if (name.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Your Name");
                        save.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (userName.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Your User Name");
                        save.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (email.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Your Email Address");
                        save.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (!util.isValidEmail(email.getText().toString().trim())) {
                        util.IOSDialog(context, "Please Enter a Vaild Email Address");
                        save.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else if (phone.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Your Phone");
                        save.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                    } else {
                        EDIT_API();
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    private void options() {
        final Item[] items = {
                new Item(getResources().getString(R.string.Camera), android.R.drawable.ic_menu_camera),
                new Item(getResources().getString(R.string.Choose_from_Gallery), android.R.drawable.ic_menu_gallery),
                new Item(getResources().getString(R.string.Cancel), android.R.drawable.ic_notification_clear_all),
        };
        ListAdapter adapter = new ArrayAdapter(
                this,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setTextSize(14f);
                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };


        new AlertDialog.Builder(this)
                .setTitle("Source")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            captureImage();

                            dialog.dismiss();
                        } else if (item == 1) {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, MEDIA_TYPE_GALLERY);
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                        }
                    }
                }).show();
    }


    public static class Item {
        public final String text;
        public final int icon;

        public Item(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private void captureImage() {
        //useful in android naught 7.0
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, MEDIA_TYPE_CAPTURE);
        } else {
            //Toast.makeText(mainActivity, "No Camera Found", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEDIA_TYPE_GALLERY) {
            if (resultCode == RESULT_OK) {
                selectedimage = getAbsolutePath(context, data.getData());

                Glide.with(this).load(selectedimage).into(image);

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == MEDIA_TYPE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                bitmap(fileUri);
                selectedimage = util.getPath(this, fileUri);
                Glide.with(this).load(selectedimage).into(image);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void bitmap(Uri resultUri) {
        Bitmap bitmap = null;
        try {
            bitmap = (MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private void EDIT_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.PROFILE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.USER_NAME, userName.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, email.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllTillyAPIs.EDIT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setName(name.getText().toString().trim());
                            savePref.setEmail(body.getString("email"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setImage(body.optString("profile"));
                            savePref.setUserName(body.optString("username"));
                            finish();
                            util.showToast(context, jsonMainobject.getString("message"));
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
