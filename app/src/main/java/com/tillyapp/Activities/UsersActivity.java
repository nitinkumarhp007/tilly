    package com.tillyapp.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.tillyapp.Adapters.UserAdapter;
import com.tillyapp.Models.ContactsList;
import com.tillyapp.R;
import com.tillyapp.Util.Parameters;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsyncArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UsersActivity extends AppCompatActivity {
    private final String TAG = "Contacts";
    UsersActivity context;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.add)
    Button add;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.search_bar)
    EditText searchBar;
    private ProgressDialog progressDialog;
    private ArrayList<ContactsList> StoreContacts;
    private ContactsList contactsList;
    UserAdapter adapter = null;
    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        ButterKnife.bind(this);

        context = UsersActivity.this;
        savePref = new SavePref(context);
        StoreContacts = new ArrayList<>();
        /*ArrayList to store the phone contacts*/



      /*  new IOSDialog.Builder(context)
               // .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("App would like to access your contacts. We will only collect All Names & phone numbers from your contact list. It will help identify your contacts using the app.").setPositiveButton("Allow", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StoreContacts = new ArrayList<ContactsList>();

                *//*Permission Check-> to read the contacts from phone *//*
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS},
                        1);
            }
        })
                .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).show();*/


        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null)
                    adapter.filter(s.toString().trim());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        requestContactPermission();

    }

    public void requestContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
              /*  if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.READ_CONTACTS)) {

                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_CONTACTS},
                            PERMISSIONS_REQUEST_READ_CONTACTS);
                }*/

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Read Contacts permission");
                builder.setPositiveButton("Allow", null);
                builder.setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                builder.setMessage("App would like to access your contacts. We will only collect All Names & phone numbers from your contact list. It will help identify your contacts using the app.");
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(
                                new String[]
                                        {android.Manifest.permission.READ_CONTACTS}
                                , PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                });
                builder.show();

            } else {
                new LongOperation().execute("");
            }
        } else {
            new LongOperation().execute("");
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    new LongOperation().execute("");


                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(UsersActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /*Asyn task to fetch the contacts from phone */
    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            /*method to load the contacts in background */
            getContactList();
            return "Loading";                      // Loader run till the contacts could not find the contacts
        }

        @Override
        protected void onPostExecute(String result) {                // Load the contacts into arraylist after contacts successfully loaded

            JSONArray array = new JSONArray();
            for (int i = 0; i < StoreContacts.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {
                    String phone_n = StoreContacts.get(i).getPhone().replaceAll("[^-?0-9]+", "");
                    jsonObject.put("name", StoreContacts.get(i).getName());
                    jsonObject.put("phone", phone_n.replace("(", "").replace(")", "").replace("-", ""));
                    array.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.e("list___", array.toString());

            if (array.length() > 0)
                SYNC(array.toString());


            progressDialog.dismiss();

        }

        @Override
        protected void onPreExecute() {
            progressDialog = util.initializeProgress(context);
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    /*method to fetch the contacts from phone */
    private void getContactList() {

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        //if ((cur != null ? cur.getCount() : 0) > 0) {
        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        try {

                            contactsList = new ContactsList();

                            /*Add name & Phone number into arraylist*/
                            contactsList.setName(name);
                            contactsList.setPhone(phoneNo);
                            contactsList.setCheck(false);
                            StoreContacts.add(contactsList);

                            // Ascending Order
                            Collections.sort(StoreContacts, new Comparator<ContactsList>() {
                                @Override
                                public int compare(ContactsList o1, ContactsList o2) {
                                    return (int) (o1.getName().compareTo(o2.getName()));
                                }
                            });

                            // adapter.notifyDataSetChanged();

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "Name: " + name);
                        Log.i(TAG, "Phone Number: " + phoneNo);
                    }
                    //  progressDialog.dismiss();
                }
            }

        }
        if (cur != null) {
            cur.close();
            progressDialog.dismiss();
        }
    }

    @OnClick({R.id.back, R.id.add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.add:
                AddButtonTask();
                break;
        }
    }

    private void AddButtonTask() {
        util.hideKeyboard(context);
        String friend_ids = "";
       /* ArrayList<ContactsList> StoreContacts_new = new ArrayList<>();
        StoreContacts_new.clear();*/
        for (int i = 0; i < StoreContacts.size(); i++) {

            boolean add = true;
            for (int j = 0; j < util.StoreContacts.size(); j++) {
                if (util.StoreContacts.get(j).getId().equals(StoreContacts.get(i).getId()))
                    add = false;
            }
            if (add) {
                if (StoreContacts.get(i).isCheck()) {
                    if (friend_ids.isEmpty()) {
                        friend_ids = StoreContacts.get(i).getId();
                    } else {
                        friend_ids = friend_ids + "," + StoreContacts.get(i).getId();
                    }
                    util.StoreContacts.add(StoreContacts.get(i));
                }
            }

        }
        if (!friend_ids.isEmpty()) {
            Intent intent = getIntent();
            intent.putExtra("friend_ids", friend_ids);
            //intent.putParcelableArrayListExtra("list", StoreContacts_new);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            util.IOSDialog(context, "Please Select User to Processed");
        }
    }

    private void SYNC(String json) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "100");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncArray mAsync = new GetAsyncArray(context, AllTillyAPIs.SYNC, formBody, savePref.getAuthorization_key(), json) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                StoreContacts.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONArray data = jsonMainobject.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                ContactsList contactsList = new ContactsList();
                                contactsList.setName(object.getString("name"));
                                contactsList.setId(object.getString("id"));
                                contactsList.setPhone(object.getString("phone"));
                                contactsList.setProfile(object.getString("profile"));
                                StoreContacts.add(contactsList);
                            }

                            if (StoreContacts.size() > 0) {
                                adapter = new UserAdapter(context, true, StoreContacts);        // set adapter for contacts
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                                errorText.setVisibility(View.GONE);
                                add.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.VISIBLE);
                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                add.setVisibility(View.GONE);
                                errorText.setVisibility(View.VISIBLE);
                            }


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
