package com.tillyapp.Activities;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tillyapp.MainActivity;
import com.tillyapp.R;
import com.tillyapp.Util.AlarmBroadcastReceiver;
import com.tillyapp.Util.ConnectivityReceiver;
import com.tillyapp.Util.Parameters;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignupActivity extends AppCompatActivity {


    @BindView(R.id.back__1_button)
    ImageView back__1_button;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.user_name)
    EditText user_name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.sign_up)
    Button signUp;
    @BindView(R.id.p_1)
    LinearLayout p_1;
    @BindView(R.id.p_2)
    LinearLayout p_2;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.term_check)
    CheckBox termCheck;
    @BindView(R.id.term_check_text)
    TextView termCheckText;

    SignupActivity context;
    private SavePref savePref;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        context = SignupActivity.this;
        savePref = new SavePref(context);


        // Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
        // PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE);


        /*if (pendingIntent == null) {
            Log.e("Alerm_scheduled_already", "Alarm is already active");
        } else {
            Log.e("Alerm_scheduled_already", "Alarm is Not active");
            setAlarm();
        }*/
        setAlarm();
    }

    public void setAlarm() {
        Intent intentToFire = new Intent(getApplicationContext(), AlarmBroadcastReceiver.class);
        intentToFire.setAction(AlarmBroadcastReceiver.ACTION_ALARM);

        // pass something
        // Bundle bundle = new Bundle();
        // bundle.putString("TAG", "FOO");
        // intentToFire.putExtra("BUNDLE", bundle);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(),
                0, intentToFire, 0);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().
                getSystemService(Context.ALARM_SERVICE);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, 7);
        long time = c.getTimeInMillis();


        int currentapiVersion = Build.VERSION.SDK_INT;
        // if current version is M o sar greater than M
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            // Wakes up the device in Doze Mode
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, time, alarmIntent);
            Log.e("Alerm_scheduled_already", "Doze Mode");
        } else if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            // Wakes up the device in Idle Mode
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, alarmIntent);
            Log.e("Alerm_scheduled_already", "Idle Mode");
        } else {
            // Old APIs
            Log.e("Alerm_scheduled_already", "Old APIs");
            alarmManager.set(AlarmManager.RTC, time, alarmIntent);
        }
        Log.e("Alerm_scheduled_already", "code run all");
    }

    @OnClick({R.id.back__1_button, R.id.sign_up, R.id.sign_in, R.id.term_check_text})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sign_up:
                SignupProcess();
                break;
            case R.id.sign_in:
                startActivity(new Intent(this, LoginActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.back__1_button:
                finish();
                break;
            case R.id.term_check_text:
                Intent intent = new Intent(context, PrivacyActivity.class);
                //    intent.putExtra("type", "about");
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SignupProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Your Name");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (user_name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter User Name");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (email.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(email.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone Number");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (confirmPassword.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Confirm Password");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
                util.IOSDialog(context, "Passward Not Match.");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!termCheck.isChecked()) {
                util.IOSDialog(context, "Please Accept Privacy Policy");
                signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                USER_SIGNUP_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void USER_SIGNUP_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, email.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.USER_NAME, user_name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(SignupActivity.this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllTillyAPIs.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setAuthorization_key(body.getString("authorization_key"));
                            savePref.setEmail(email.getText().toString().trim());
                            savePref.setPhone(phone.getText().toString().trim());
                            savePref.setUserName(user_name.getText().toString().trim());
                            savePref.setID("");
                            savePref.setName(name.getText().toString().trim());
                            //savePref.setImage(body.getString("profile"));
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "User Registered Sucessfully!!!");
                            util.showToast(context, "Welcome to Tilly App");

                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
