package com.tillyapp.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tillyapp.Adapters.UserSearchAdapter;
import com.tillyapp.Models.ContactsList;
import com.tillyapp.R;
import com.tillyapp.Util.Parameters;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SearchUsersActivity extends AppCompatActivity {
    SearchUsersActivity context;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_text)
    TextView errorText;

    private SavePref savePref;
    private ArrayList<ContactsList> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_users);
        ButterKnife.bind(this);

        context = SearchUsersActivity.this;
        savePref = new SavePref(context);

        searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    SEARCH(v.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });
    }

    private void SEARCH(String user_name) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllTillyAPIs.SEARCH + "?username=" + user_name, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray data = jsonmainObject.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                ContactsList contactsList = new ContactsList();
                                contactsList.setName(object.getString("name"));
                                contactsList.setId(object.getString("id"));
                                contactsList.setPhone(object.getString("phone"));
                                contactsList.setProfile(object.getString("profile"));
                                list.add(contactsList);
                            }

                            if (list.size() > 0) {
                                UserSearchAdapter adapter = new UserSearchAdapter(SearchUsersActivity.this, false, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                errorText.setText("No User Found!");
                                errorText.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);

                            }
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void AddButtonTask(String id) {
        util.hideKeyboard(context);
        boolean add = true;
        for (int i = 0; i < util.StoreContacts.size(); i++) {
            if (util.StoreContacts.get(i).getId().equals(id))
                add = false;
        }
        Intent intent = getIntent();
        if (add) {
            int pos = 0;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getId().equals(id))
                    pos = i;
            }
            intent.putExtra("friend_ids", list.get(pos).getId());
            util.StoreContacts.add(list.get(pos));
        } else {
            intent.putExtra("friend_ids", "");
        }
        setResult(RESULT_OK, intent);
        finish();
    }


    @OnClick(R.id.back)
    public void onViewClicked() {
        finish();
    }
}
