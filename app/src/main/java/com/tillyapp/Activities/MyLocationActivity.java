package com.tillyapp.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.tillyapp.R;
import com.tillyapp.Util.AppController;
import com.tillyapp.Util.ConnectivityReceiver;
import com.tillyapp.Util.Parameters;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsync;
import com.tillyapp.parser.GetAsyncGet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MyLocationActivity extends AppCompatActivity {
    @BindView(R.id.save)
    Button save;
    @BindView(R.id.home)
    TextView home;
    @BindView(R.id.office)
    TextView office;
    @BindView(R.id.back)
    ImageView back;
    private final static int PLACE_PICKER_REQUEST = 34234;
    boolean is_office = false;

    String lat_home = "", lng_home = "";
    String lat_office = "", lng_office = "";

    MyLocationActivity context;
    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_location);

        ButterKnife.bind(this);

        context = MyLocationActivity.this;
        savePref = new SavePref(context);


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_office = false;
                place_picker();
            }
        });

        office.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_office = true;
                place_picker();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!home.getText().toString().trim().isEmpty() || !office.getText().toString().trim().isEmpty()) {
                    ADDRESS_API();
                }
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            GET_ADDRESS_API();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    private void GET_ADDRESS_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllTillyAPIs.ADDRESS, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject data = jsonmainObject.getJSONObject("data");
                            if (data.length() > 0) {

                                if (!data.getString("home_location").isEmpty()) {
                                    home.setText(data.getString("home_location"));
                                    lat_home = data.getString("home_latitude");
                                    lng_home = data.getString("home_longitude");
                                }

                                if (!data.getString("office_location").isEmpty()) {
                                    office.setText(data.getString("office_location"));
                                    lat_office = data.getString("office_latitude");
                                    lng_office = data.getString("office_longitude");
                                }
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void ADDRESS_API() {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!lat_home.isEmpty()) {
            formBuilder.addFormDataPart(Parameters.HOME_LOCATION, home.getText().toString().trim());
            formBuilder.addFormDataPart(Parameters.HOME_LATITUDE, lat_home);
            formBuilder.addFormDataPart(Parameters.HOME_LONGITUDE, lng_home);
        }
        if (!lat_office.isEmpty()) {
            formBuilder.addFormDataPart(Parameters.OFFICE_LOCATION, office.getText().toString().trim());
            formBuilder.addFormDataPart(Parameters.OFFICE_LATITUDE, lat_office);
            formBuilder.addFormDataPart(Parameters.OFFICE_LONGITUDE, lng_office);
        }

        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(AppController.getInstance(), AllTillyAPIs.ADDRESS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            util.showToast(context, "Address Saved Sucessfully!");
                            finish();
                        } else {
                            util.IOSDialog(AppController.getInstance(), jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = Autocomplete.getPlaceFromIntent(data);



                if (!is_office) {
                    home.setText(place.getName());
                    lat_home = String.valueOf(place.getLatLng().latitude);
                    lng_home = String.valueOf(place.getLatLng().longitude);
                } else {
                    office.setText(place.getName());
                    lat_office = String.valueOf(place.getLatLng().latitude);
                    lng_office = String.valueOf(place.getLatLng().longitude);
                }

            }
        }
    }

    private void place_picker() {
        // Initialize Places.
        Places.initialize(getApplicationContext(), "AIzaSyAA-k3KpN8PbS5u4_9qLFGIFZ_fIm52iM4");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);
        // Set the fields to specify which types of place data to return.
        List<com.google.android.libraries.places.api.model.Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID,
                com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.ADDRESS, com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.PHONE_NUMBER, com.google.android.libraries.places.api.model.Place.Field.RATING, com.google.android.libraries.places.api.model.Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(this);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);


    }
}
