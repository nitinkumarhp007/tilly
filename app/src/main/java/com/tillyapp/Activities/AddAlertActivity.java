package com.tillyapp.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.tillyapp.Adapters.UserAdapter;
import com.tillyapp.R;
import com.tillyapp.Util.ConnectivityReceiver;
import com.tillyapp.Util.Parameters;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsync;
import com.tillyapp.parser.GetAsyncGet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddAlertActivity extends AppCompatActivity {

    AddAlertActivity context;

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.add_user)
    Button addUser;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.add)
    Button add;

    String latitude = "", longitude = "", friend_ids = "";

    private final int PLACE_PICKER_REQUEST = 34234;
    @BindView(R.id.title___)
    EditText title;

    private SavePref savePref;

    String location_home = "", lat_home = "", lng_home = "";
    String location_office = "", lat_office = "", lng_office = "";

    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alert);
        ButterKnife.bind(this);

        context = AddAlertActivity.this;
        savePref = new SavePref(context);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            ADDRESS_API();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }


    private void ADDRESS_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllTillyAPIs.ADDRESS, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject data = jsonmainObject.getJSONObject("data");
                            if (data.length() > 0) {

                                if (!data.getString("home_location").isEmpty()) {
                                    location_home = data.getString("home_location");
                                    lat_home = data.getString("home_latitude");
                                    lng_home = data.getString("home_longitude");
                                }

                                if (!data.getString("office_location").isEmpty()) {
                                    location_office = data.getString("office_location");
                                    lat_office = data.getString("office_latitude");
                                    lng_office = data.getString("office_longitude");
                                }
                            }
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick({R.id.back, R.id.location, R.id.add_user, R.id.add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.location:
                if (location_home.isEmpty() && location_office.isEmpty()) {
                    Add("Add Home Address", "Add Office Address");
                } else if (!location_home.isEmpty() && !location_office.isEmpty()) {
                    Add(location_home, location_office);
                } else if (location_home.isEmpty()) {
                    Add("Add Home Address", location_office);
                } else if (location_office.isEmpty()) {
                    Add(location_home, "Add Office Address");
                }

                break;
            case R.id.add_user:
                add_user();
                break;
            case R.id.add:
                if (ConnectivityReceiver.isConnected()) {
                    /*if (title.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Title");
                    } else*/
                    if (location.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Add Location");
                    } else if (friend_ids.isEmpty()) {
                        util.IOSDialog(context, "Please Add User");
                    } else {
                        ALERT_API();
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    private void add_user() {
        CharSequence[] items = {"Choose From Phone Book", "Search by Username/phone"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    startActivityForResult(new Intent(context, UsersActivity.class), 200);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                } else if (item == 1) {
                    startActivityForResult(new Intent(context, SearchUsersActivity.class), 200);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void Add(final String home, final String office) {
        CharSequence[] items = {"Choose new Address", home, office};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    place_picker();
                } else if (item == 1) {
                    if (!home.equals("Add Home Address")) {
                        latitude = lat_home;
                        longitude = lng_home;
                        location.setText(home);
                    } else {
                        startActivity(new Intent(AddAlertActivity.this, MyLocationActivity.class));
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                } else {
                    if (!office.equals("Add Office Address")) {
                        latitude = lat_office;
                        longitude = lng_office;
                        location.setText(office);
                    } else {
                        startActivity(new Intent(AddAlertActivity.this, MyLocationActivity.class));
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void ALERT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.LOCATIONS, location.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
        formBuilder.addFormDataPart(Parameters.TITLE, title.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);
        formBuilder.addFormDataPart(Parameters.FRIEND_IDS, friend_ids);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllTillyAPIs.ALERT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setMessage("Alert Added Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();

                            showAdd();


                        } else {
                            util.showToast(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void showAdd() {

        mInterstitialAd = new InterstitialAd(this);
        //  mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_app_id));
        // demo credential
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitial_full_screen));

        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdClosed() {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                Log.e("add___", "show_");
                //Util.is_add_closed = true;
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Util.is_add_closed = false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                location.setText(place.getName());
                latitude = String.valueOf(place.getLatLng().latitude);
                longitude = String.valueOf(place.getLatLng().longitude);

            } else if (requestCode == 200) {

                //StoreContacts = data.getParcelableArrayListExtra("list");
                if (util.StoreContacts.size() > 0) {
                    if (!friend_ids.isEmpty()) {
                        friend_ids = friend_ids + "," + data.getStringExtra("friend_ids");
                    } else {
                        friend_ids = data.getStringExtra("friend_ids");
                    }

                    UserAdapter adapter = new UserAdapter(context, false, util.StoreContacts);
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myRecyclerView.setAdapter(adapter);

                }
            }
        }
    }

    private void place_picker() {
        // Initialize Places.
        Places.initialize(getApplicationContext(), "AIzaSyAA-k3KpN8PbS5u4_9qLFGIFZ_fIm52iM4");
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID,
                Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS, Place.Field.ID, Place.Field.PHONE_NUMBER, Place.Field.RATING, Place.Field.WEBSITE_URI);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(this);

        startActivityForResult(intent, PLACE_PICKER_REQUEST);


    }
}
