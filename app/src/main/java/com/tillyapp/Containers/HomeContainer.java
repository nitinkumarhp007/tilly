package com.tillyapp.Containers;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tillyapp.Fragments.HomeFragment;
import com.tillyapp.R;

public class HomeContainer extends Fragment_Container {
    private boolean mIsViewInited = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("OnCreate view", "for Tab4");
        return inflater.inflate(R.layout.container_for_tab, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.e("on Activity created", "for Tab4");
        super.onActivityCreated(savedInstanceState);
        if (!mIsViewInited) {
            mIsViewInited = true;
            onIntialView();
        }
    }

    private void onIntialView() {

        replaceFragment(new HomeFragment(), false);

    }
}


