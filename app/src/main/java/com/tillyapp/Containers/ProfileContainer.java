package com.tillyapp.Containers;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tillyapp.Fragments.ProfileFragment;
import com.tillyapp.R;


/**
 * Created by android on 16/3/17.
 */

public class ProfileContainer extends Fragment_Container {
    private boolean mIsViewInited;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("OnCreate view", "for Tab3");
        return inflater.inflate(R.layout.container_for_tab, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.e("on Activity created", "for Tab3");
        super.onActivityCreated(savedInstanceState);
        if (!mIsViewInited) {
            mIsViewInited = true;
            onIntialView();
        }
    }

    private void onIntialView() {

        replaceFragment(new ProfileFragment(), false);
    }
}


