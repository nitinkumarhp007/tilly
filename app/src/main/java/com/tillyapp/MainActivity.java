package com.tillyapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.FragmentTabHost;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.tillyapp.Containers.Fragment_Container;
import com.tillyapp.Containers.HomeContainer;
import com.tillyapp.Containers.NotificationContainer;
import com.tillyapp.Containers.ProfileContainer;
import com.tillyapp.Util.AlarmBroadcastReceiver;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;

import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public final String TAB_1_TAG = "tab_1";
    public final String TAB_2_TAG = "tab_2";
    public final String TAB_3_TAG = "tab_3";
    /* public final String TAB_4_TAG = "tab_4";*/
    public FragmentTabHost mFragmentTabHost;
    public static MainActivity context;
    private LayoutInflater inflater = null;
    public TabWidget tabs;

    public static TextView title;
    public static ImageView setting;
    public static androidx.appcompat.widget.Toolbar toolbar;

    private AdView mAdView;

    boolean is_from_push = false;


    private GoogleApiClient googleApiClient;
    final static int REQUEST_LOCATION = 199;
    SavePref savePref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        settingtabhost();
    }

    private void settingtabhost() {



        context = MainActivity.this;
        savePref = new SavePref(context);

        Log.e("device_token_", SavePref.getDeviceToken(this, "token"));
        Log.e("device_token_", "auth" + savePref.getAuthorization_key());


        title = (TextView) findViewById(R.id.title);
        setting = (ImageView) findViewById(R.id.setting);
        toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tabs = (TabWidget) findViewById(android.R.id.tabs);
        mFragmentTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mFragmentTabHost.setup(context, getSupportFragmentManager(), R.id.realtabcontent);// getchildFragment for calling First Tab fragment from home fragment
        //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View viewHome = prepareTabView(R.drawable.tab_background_for_home, "Home");
        TabHost.TabSpec tabSpec1 = mFragmentTabHost.newTabSpec(TAB_1_TAG).setIndicator(viewHome);
        mFragmentTabHost.addTab(tabSpec1, HomeContainer.class, null);

        /*View viewMessage = prepareTabView(R.drawable.tab_background_for_alert, "Alert");
        TabHost.TabSpec tabSpec2 = mFragmentTabHost.newTabSpec(TAB_2_TAG).setIndicator(viewMessage);
        mFragmentTabHost.addTab(tabSpec2, AlertsContainer.class, null);*/

        View viewSearch = prepareTabView(R.drawable.tab_background_for_noti, "Notifications");
        TabHost.TabSpec tabSpec2 = mFragmentTabHost.newTabSpec(TAB_2_TAG).setIndicator(viewSearch);
        mFragmentTabHost.addTab(tabSpec2, NotificationContainer.class, null);

        View viewS = prepareTabView(R.drawable.tab_background_for_profile, "Profile");
        TabHost.TabSpec tabSpec4 = mFragmentTabHost.newTabSpec(TAB_3_TAG).setIndicator(viewS);
        mFragmentTabHost.addTab(tabSpec4, ProfileContainer.class, null);

        is_from_push = getIntent().getBooleanExtra("is_from_push", false);

        if (is_from_push) {
            mFragmentTabHost.setCurrentTab(1);
            util.IOSDialog(context, getIntent().getStringExtra("message"));
            savePref.setPushMessage("");
        } else {
            mFragmentTabHost.setCurrentTab(0);
        }


        mFragmentTabHost.getTabWidget().setDividerDrawable(null);

        mFragmentTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                Log.e("click", "here tabId : " + tabId);
            }
        });

      /*  Intent intent = new Intent(context, AlarmBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE);

        if (pendingIntent == null) {
            Log.e("Alerm_scheduled_already", "Alarm is already active");
        } else {
            Log.e("Alerm_scheduled_already", "Alarm is Not active");
            setAlarm();
        }*/
        AddTask();

        Location_tracker();

    }

    private void Location_tracker() {
        // Todo Location Already on  ... start
        final LocationManager manager = (LocationManager) MainActivity.this.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(MainActivity.this)) {
            //Toast.makeText(MainActivity.this, "Gps already enabled", Toast.LENGTH_SHORT).show();
            //finish();
        }
        // Todo Location Already on  ... end

        if (!hasGPSDevice(MainActivity.this)) {
            //Toast.makeText(MainActivity.this, "Gps not Supported", Toast.LENGTH_SHORT).show();
        }

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(MainActivity.this)) {
            Log.e("keshav", "Gps already enabled");
            // Toast.makeText(MainActivity.this, "Gps not enabled", Toast.LENGTH_SHORT).show();
            enableLoc();
        } else {
            Log.e("keshav", "Gps already enabled");
            //Toast.makeText(MainActivity.this, "Gps already enabled", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(MainActivity.this, REQUEST_LOCATION);

                                //finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
        }
    }

    private void AddTask() {
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        mAdView = (AdView) findViewById(R.id.adView);


        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("C04B1BFFB0774708339BC273F8A43708")
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                // Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                // Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }

    public void setAlarm() {
        Intent intentToFire = new Intent(getApplicationContext(), AlarmBroadcastReceiver.class);
        intentToFire.setAction(AlarmBroadcastReceiver.ACTION_ALARM);

        // pass something
        // Bundle bundle = new Bundle();
        // bundle.putString("TAG", "FOO");
        // intentToFire.putExtra("BUNDLE", bundle);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(),
                0, intentToFire, 0);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().
                getSystemService(Context.ALARM_SERVICE);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, 7);
        long time = c.getTimeInMillis();


        int currentapiVersion = Build.VERSION.SDK_INT;
        // if current version is M o sar greater than M
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            // Wakes up the device in Doze Mode
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, time, alarmIntent);
        } else if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            // Wakes up the device in Idle Mode
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, alarmIntent);
        } else {
            // Old APIs
            alarmManager.set(AlarmManager.RTC, time, alarmIntent);
        }
        Log.e("Alerm_scheduled_already", "code run all");
    }

    private View prepareTabView(int drawable, String text) {
        View view = inflater.inflate(R.layout.layout_tab, null);
        ImageView tab_image = (ImageView) view.findViewById(R.id.tab_image);
        TextView tab_text = (TextView) view.findViewById(R.id.tab_text);
        tab_text.setText(text);
        tab_image.setBackgroundResource(drawable);
        return view;
    }

    @Override
    public void onBackPressed() {
        try {
            tabs.setVisibility(View.VISIBLE);
            boolean isPopFragment = false;
            String currentTabTag = mFragmentTabHost.getCurrentTabTag();
            if (currentTabTag.equals(TAB_1_TAG)) {
                isPopFragment = ((Fragment_Container) getSupportFragmentManager().findFragmentByTag(TAB_1_TAG)).popFragment();
            } else if (currentTabTag.equals(TAB_2_TAG)) {
                isPopFragment = ((Fragment_Container) getSupportFragmentManager().findFragmentByTag(TAB_2_TAG)).popFragment();
            } else if (currentTabTag.equals(TAB_3_TAG)) {
                isPopFragment = ((Fragment_Container) getSupportFragmentManager().findFragmentByTag(TAB_3_TAG)).popFragment();
            }/* else if (currentTabTag.equals(TAB_4_TAG)) {
                isPopFragment = ((Fragment_Container) getSupportFragmentManager().findFragmentByTag(TAB_4_TAG)).popFragment();
            }*/
            if (!isPopFragment) {
                finish();
            }
        } catch (Exception ex) {
            super.onBackPressed();
        }
    }

    public void setCurrentTab(int tab_index) {
        mFragmentTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mFragmentTabHost.setCurrentTab(tab_index);
    }
}
