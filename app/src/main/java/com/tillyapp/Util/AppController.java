package com.tillyapp.Util;

import android.app.Application;
import android.content.Intent;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.android.gms.ads.MobileAds;
import com.tillyapp.R;

import java.util.concurrent.TimeUnit;


public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Intent intent = new Intent(mInstance, NetworkServices.class);
        mInstance.startService(intent);

        PeriodicWorkRequest myWork = new PeriodicWorkRequest.Builder(UploadWorker.class, 15, TimeUnit.MINUTES).build();
        WorkManager.getInstance().enqueue(myWork);

        MobileAds.initialize(this, getString(R.string.admob_app_id));

    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}