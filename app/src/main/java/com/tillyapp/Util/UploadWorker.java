package com.tillyapp.Util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.NonNull;
import android.util.Log;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.tillyapp.services.GetLocationService;

public class UploadWorker extends Worker {

    public UploadWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    @Override
    public Result doWork() {
        // Do the work here--in this case, upload the images.
        Log.e("service", "worker_calls");
        // use this to start and trigger a service

      /*  Intent i = new Intent(AppController.getInstance(), GetLocationService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AppController.getInstance().startForegroundService(i);
        } else {
            AppController.getInstance().startService(i);
        }*/

        GetLocationService mYourService = new GetLocationService();
        Intent mServiceIntent = new Intent(AppController.getInstance(), GetLocationService.class);
        if (!isMyServiceRunning(mYourService.getClass())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                AppController.getInstance().startForegroundService(mServiceIntent);
            } else {
                AppController.getInstance().startService(mServiceIntent);
            }
        }


        return Result.success();
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) AppController.getInstance().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("Service status", "Running");
                return true;
            }
        }
        Log.i ("Service status", "Not running");
        return false;
    }
}