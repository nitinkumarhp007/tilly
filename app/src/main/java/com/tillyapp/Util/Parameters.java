package com.tillyapp.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "authorization_key";
    public static final String AUTH_KEY = "auth_key";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String EMAIL = "email";
    public static final String PROFILE = "profile";
    public static final String SOCIAL_TYPE = "social_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "device_type";
    public static final String NAME = "name";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String DOB = "dob";
    public static final String PHONE = "phone";
    public static final String USER_NAME = "username";
    public static final String LOCATIONS = "locations";
    public static final String LATITUDE = "latitude";

    public static final String HOME_LOCATION = "home_location";
    public static final String HOME_LATITUDE = "home_latitude";
    public static final String HOME_LONGITUDE = "home_longitude";
    public static final String OFFICE_LOCATION = "office_location";
    public static final String OFFICE_LATITUDE = "office_latitude";
    public static final String OFFICE_LONGITUDE = "office_longitude";
    public static final String TITLE = "title";
    public static final String LONGITUDE = "longitude";
    public static final String FRIEND_IDS = "friend_ids";


}















