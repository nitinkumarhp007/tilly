package com.tillyapp.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tillyapp.Models.ContactsList;
import com.tillyapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class UserAdapter extends RecyclerView.Adapter<UserAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;

    private View view;
    boolean b;
    ArrayList<ContactsList> tempList;
    ArrayList<ContactsList> list;

    public UserAdapter(Context context, boolean b, ArrayList<ContactsList> storeContacts) {
        this.context = context;
        this.b = b;
        this.tempList = storeContacts;
        this.list = storeContacts;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.user_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        if (b) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.remove.setVisibility(View.GONE);
        } else {
            holder.checkBox.setVisibility(View.GONE);
            holder.remove.setVisibility(View.VISIBLE);
        }

        Glide.with(context).load(list.get(position).getProfile()).into(holder.image);

        holder.name.setText(list.get(position).getName());
        holder.phone.setText(list.get(position).getPhone());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    list.get(position).setCheck(true);
                } else {
                    list.get(position).setCheck(false);
                }
            }
        });

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.check_box)
        CheckBox checkBox;
        @BindView(R.id.remove)
        ImageView remove;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ContactsList> nList = new ArrayList<ContactsList>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ContactsList wp : tempList) {
                String s = wp.getName().toLowerCase() + " " + wp.getPhone();
                if (s.contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }

}
