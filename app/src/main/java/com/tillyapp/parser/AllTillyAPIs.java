package com.tillyapp.parser;


public class AllTillyAPIs {

    //public static final String BASE_URL = "http://18.223.80.65:4000/apis/v1/";
    public static final String BASE_URL = "https://app.tilly-app.com/apis/v1/";

    public static final String USERLOGIN = BASE_URL + "user/login";
    public static final String USER_SIGNUP = BASE_URL + "user";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot_password";
    public static final String LOGOUT = BASE_URL + "user/logout";
    public static final String CHANGEPASSWORD = BASE_URL + "user/change_password";;
    public static final String ALERT = BASE_URL + "alert";
    public static final String EDIT = BASE_URL + "user/edit";
    public static final String SYNC = BASE_URL + "alert/sync";
    public static final String SEND = BASE_URL + "alert/send";
    public static final String NOTIFICATION = BASE_URL + "notification";
    public static final String ADDRESS = BASE_URL + "address";
    public static final String APP_INFO = BASE_URL + "app_info";
    public static final String SEARCH = BASE_URL + "search";
}
