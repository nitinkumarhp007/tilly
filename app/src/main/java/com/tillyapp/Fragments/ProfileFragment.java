package com.tillyapp.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tillyapp.Activities.EditProfileActivity;
import com.tillyapp.Activities.SettingActivity;
import com.tillyapp.MainActivity;
import com.tillyapp.R;
import com.tillyapp.Util.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.change_profile_pic)
    Button changeProfilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.update)
    Button update;
    Unbinder unbinder;
    @BindView(R.id.user_name)
    TextView userName;

    private SavePref savePref;
    Context context;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;

    }

    private void setdata() {
        context = getActivity();
        savePref = new SavePref(context);

        name.setText(savePref.getName());
        phone.setText(savePref.getPhone());
        email.setText(savePref.getEmail());
        userName.setText(savePref.getUserName());
        if (!savePref.getImage().isEmpty())
            Glide.with(context).load(savePref.getImage()).into(image);
        else
            Glide.with(context).load(R.drawable.user).into(image);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.toolbar.setVisibility(View.VISIBLE);
        MainActivity.title.setText("Profile");
        MainActivity.setting.setVisibility(View.VISIBLE);
        MainActivity.setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SettingActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

        setdata();
    }

    @OnClick({R.id.change_profile_pic, R.id.update})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.change_profile_pic:

                break;
            case R.id.update:
                startActivity(new Intent(getActivity(), EditProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }
}
