package com.tillyapp.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.tillyapp.Activities.AddAlertActivity;
import com.tillyapp.Activities.LoginActivity;
import com.tillyapp.Adapters.HomeAdapter;
import com.tillyapp.MainActivity;
import com.tillyapp.Models.HomeModel;
import com.tillyapp.R;
import com.tillyapp.Util.ConnectivityReceiver;
import com.tillyapp.Util.Parameters;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsyncDELETE;
import com.tillyapp.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.add_new_alert)
    Button addNewAlert;
    Unbinder unbinder;
    @BindView(R.id.error_text)
    TextView errorText;

    private int offset = 1;
    Context context;
    private SavePref savePref;
    private HomeAdapter adapter = null;

    private ArrayList<HomeModel> list;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);


        return view;
    }


    private void ALERT_API(String offset) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllTillyAPIs.ALERT + "/" + offset + "?limit=10000", formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                HomeModel homeModel = new HomeModel();
                                homeModel.setId(object.getString("id"));
                                homeModel.setCreated(object.getString("created"));
                                homeModel.setTitle(object.getString("title"));
                                homeModel.setStatus(object.getString("status"));
                                //homeModel.setFriends(object.getString("name"));
                                homeModel.setLocation(object.getString("locations"));
                                list.add(homeModel);

                            }
                            if (list.size() > 0) {
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                                adapter = new HomeAdapter(getActivity(), list, HomeFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                myRecyclerView.setAdapter(adapter);
                            } else {
                                errorText.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }

                           /* if (!util.Push_message.isEmpty()) {
                                Log.e("getPushMessage", savePref.getPushMessage());
                                util.IOSDialog(context, savePref.getPushMessage());
                                util.Push_message="";
                            }*/

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DELETEAlert(final int position) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Delete?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_ADDRESS(list.get(position).getId(), position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public void DELETE_ADDRESS(String alert_id, final int position) {
        final ProgressDialog dialog = util.initializeProgress(context);
        dialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "200");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllTillyAPIs.ALERT + "?alert_id=" + alert_id, formBody, dialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response result) {
                //list = new ArrayList<>();
                dialog.dismiss();
                if (result.isSuccessful()) {
                    Toast.makeText(context, "Alert Deleted Successfully!", Toast.LENGTH_SHORT).show();
                    list.remove(position);
                    adapter.notifyDataSetChanged();
                   /* try {
                        JSONObject jsonmainObject = new JSONObject(result.toString());
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            Toast.makeText(context, "Alert Deleted Successfully!", Toast.LENGTH_SHORT).show();
                            list.remove(position);
                            adapter.notifyDataSetChanged();
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }*/
                }
            }


            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.add_new_alert)
    public void onViewClicked() {
        util.StoreContacts = new ArrayList<>();
        util.StoreContacts.clear();
        startActivity(new Intent(getActivity(), AddAlertActivity.class));
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

    @Override
    public void onResume() {
        super.onResume();
        //  MainActivity.title.setText("Home");
        MainActivity.toolbar.setVisibility(View.GONE);
        if (ConnectivityReceiver.isConnected()) {
            offset = 1;
            ALERT_API(String.valueOf(offset));
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }
}
