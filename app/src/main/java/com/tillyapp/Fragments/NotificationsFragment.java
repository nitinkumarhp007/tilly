package com.tillyapp.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tillyapp.Activities.LoginActivity;
import com.tillyapp.Adapters.NotificationAdapter;
import com.tillyapp.MainActivity;
import com.tillyapp.Models.NotificationModel;
import com.tillyapp.R;
import com.tillyapp.Util.ConnectivityReceiver;
import com.tillyapp.Util.Parameters;
import com.tillyapp.Util.SavePref;
import com.tillyapp.Util.util;
import com.tillyapp.parser.AllTillyAPIs;
import com.tillyapp.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends Fragment {

    Context context;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Unbinder unbinder;
    @BindView(R.id.error_text)
    TextView errorText;
    int offset = 1;

    private ArrayList<NotificationModel> list;

    public NotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);


        if (ConnectivityReceiver.isConnected()) {
            offset = 1;
            NOTIFICATION_API(String.valueOf(offset));
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        return view;
    }


    private void NOTIFICATION_API(String offset) {

        TimeZone timeZone = TimeZone.getDefault();
        String time_zone = timeZone.getID();


        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "2000000");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllTillyAPIs.NOTIFICATION + "/"
                + offset + "?timeZone=" + time_zone + "&limit=1000000", formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                NotificationModel notificationModel = new NotificationModel();
                                notificationModel.setId(object.getString("id"));
                                notificationModel.setText(object.getString("text"));
                                notificationModel.setCreated(object.getString("created"));
                                list.add(notificationModel);

                            }
                            if (list.size() > 0) {
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                myRecyclerView.setAdapter(new NotificationAdapter(getActivity(), list));
                            } else {
                                errorText.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.toolbar.setVisibility(View.VISIBLE);
        MainActivity.title.setText("Notifications");
        MainActivity.setting.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
